# andres_rivera24111

Taller de Programación 2

Fechas de Evaluación

Entregable 1: 6 septiembre 2023
Entregable 2: 2 de Octubre, 2023
Parcial 1: 4 Octubre, 2023
Entregable 3: 6 Noviembre, 2023
Entregable 4: 29 Noviembre, 2023
Parcial Final: 6 Diciembre, 2023

Herramientas Utilizadas

Git: Sitio oficial
Visual Studio Code: Descargar aquí Extenciones (Python , Git grafh , Git lens)
Python 3.9.2: Python descarga

Control de Versiones

git clone 
git add .  Empaquetar cambios
git commit -m "descripcion del cambio" Etiquetar version
git push origin main Carga o empuja el paquete a la ubicacion remota

Espacio de Trabajo

python -m venv venv creacion de espacio de trabajo

.\venv\Scripts\activate activar entorno virtual

Comandos VSCode

Control + ñ Terminal

Sesiones

Sesión
Fecha
Tema

1
09/08/2023
Presentación y Concertación de evaluación

1
14/08/2023
Introducción al control de versiones y su importancia en el curso