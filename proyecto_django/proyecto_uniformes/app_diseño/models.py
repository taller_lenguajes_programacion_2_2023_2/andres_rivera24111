
from django.db import models

class Prendas(models.Model):
          
    id = models.AutoField(primary_key=True)
    tipo_prenda = models.CharField(max_length=100)
    referencia = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=100)
    planta_confeccion = models.CharField(max_length=100)
    tiempo_entrega = models.CharField(max_length=100)
    
    def __str__(self):      
        return self.descripcion
    
    def __str__(self):
        return self.id