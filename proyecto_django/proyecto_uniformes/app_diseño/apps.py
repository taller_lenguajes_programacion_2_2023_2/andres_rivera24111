from django.apps import AppConfig


class AppDiseñoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app_diseño'
