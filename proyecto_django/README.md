# Taller de Programación 2

## Fechas de Evaluación

1. **Entregable 1**: 1 de Octubre de 2023

## Herramientas Utilizadas

- **Git**: [Sitio oficial](https://git-scm.com/)
- **Visual Studio Code**: [Descargar aquí](https://code.visualstudio.com/) Extenciones (Python , Git grafh , Git lens)
- **Python 3.9.2**: [Python descarga](https://www.python.org/ftp/python/3.9.2/python-3.9.2-amd64.exe)

## Control de Versiones

- **git clone** `<url>`
- **git add** .  Empaquetar cambios
- **git commit -m "descripcion del cambio"** Etiquetar version
- **git push origin main** Carga o empuja el paquete a la ubicacion remota

## Espacio de Trabajo

- **python -m venv venv** creacion de espacio de trabajo
- **.\venv\Scripts\activate** activar entorno virtual

## Comandos VSCode

- Control + ñ Terminal

## Django

- Paso 1:
    pip install django
- Paso 2:
    django-admin startproject proyecto_uniformes
- Paso 3: prueba de ejecución framework
    python .\proyecto_uniformes\manage.py runserver
- Paso 4: crear aplicacion
    cd .\proyecto_uniformes\  y python manage.py startapp app_diseño
- Paso 5: crear modelo de datos models.py
    definir atributos y la clase del modelos de datos
- Paso 6: migrar modelo
    python manage.py migrate
- Paso 7: actualizar cambios en el modelo
    python manage.py makemigrations
    python manage.py migrate
- Paso 8: crear usuario admin de la bd
    python manage.py createsuperuser
    user:admin
    email:andres_rivera24111@elpoli.edu.co
    password: Cistokis1.

    user:admin123
    email:admin@elpoli.edu.co
    password: admin123
- Paso 9: registrar Modelo
    admin.py registrar el modelo
    from .models import Persona
    admin.site.register(Persona)
- Paso 10: la vista asociada al html

## Sesiones

| Sesión | Fecha      | Tema                                                               |
| ------- | ---------- | ------------------------------------------------------------------ |
| 1       | 25/09/2023 | Proyecto Python, conexión SQLITE3, lectura de datos archivo de excel con Panda                       |
| 2       | 27/09/2023 | Proyecto Python con Django, conexión SQLITE3, lectura de datos archivo de excel con Panda |

