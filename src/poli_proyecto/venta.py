
from persona import Persona
from producto import Producto
from conexion import Conexion

class Venta(Conexion):
    def __init__(self,id=0,fecha="13/09/2023",cantidad=0,total=0.0) -> None:
        self.__id=id
        self.__fecha=fecha
        self.__per = Persona()
        self.__prod = Producto()
        self.__cantidad=cantidad
        self.__total=total  
        super().__init__()
        self.create_Vta()
        
            
    def create_Vta(self):
        if self.crear_tabla(nom_tabla="ventas",datos_tbl="datos_vta"):
            print("Tabla Venta Creada!!!")
        
        return True
          
        
    def __str__(self) -> str:
        return "Venta \n id {} fecha {} \n   {} \n   {} \n cantidad {} total {}".format(self.__id,self.__fecha,self.__per.__str__(), self.__prod.__str__(),self.__cantidad,self.__total)
