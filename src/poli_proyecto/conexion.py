import sqlite3
import json
import re

class Conexion:
    def __init__(self) -> None:
        self.__user=""
        self.__password=""
        self.__puerto=0
        self.__url=""
        __nom_db="db_photograph.sqlite"
        self.__querys = self.obtener_json()
        self.__conex = sqlite3.connect(__nom_db)
        self.__cursor = self.__conex.cursor()
        #conex = sqlite3.connect()
    
    def obtener_json(self):
        #E:/taller_programacion2/andres_callejas05082/src/poli_proyecto/static/sql/querys.json
        ruta = "./src/poli_proyecto/static/sql/querys.json"
        querys={}
        with open(ruta, 'r') as file:
            querys = json.load(file) 
        return querys
    
    def crear_tabla(self, nom_tabla="",datos_tbl = ""):
        if nom_tabla != "" :
            datos=self.__querys[datos_tbl]
            query =  self.__querys["create_tb"].format(nom_tabla,datos)
            self.__cursor.execute(query)
            #print(query)
            return True
        else:
            return False
        
    def insertar_datos(self, nom_tabla="",nom_columns = "", datos_carga=""):
        if nom_tabla != "" :
            columnas=self.__querys[nom_columns]
            query =  self.__querys["insert"].format(nom_tabla,columnas,datos_carga)
            self.__cursor.execute(query)
            self.__conex.commit()
            #print(query)
            return True
        else:
            return False
   
# conx = Conexion()
# conex = sqlite3.connect(conx.nom_db) 

# tabla = "create table if not exists {} ( {} ) ".format("persona", " id integer primary key ")
# conex.execute(tabla)

# select = conex.execute("select * from persona ")
# print(select) 
    
    
    