-- Generado por Oracle SQL Developer Data Modeler 23.1.0.087.0806
--   en:        2023-08-23 19:22:45 COT
--   sitio:      Oracle Database 11g
--   tipo:      Oracle Database 11g



DROP TABLE cliente CASCADE CONSTRAINTS;

DROP TABLE curso CASCADE CONSTRAINTS;

DROP TABLE venta CASCADE CONSTRAINTS;

-- predefined type, no DDL - MDSYS.SDO_GEOMETRY

-- predefined type, no DDL - XMLTYPE

CREATE TABLE cliente (
    id_cliente VARCHAR2(20) NOT NULL,
    nombre     VARCHAR2(100) NOT NULL,
    apellido   VARCHAR2(100) NOT NULL,
    email      VARCHAR2(100) NOT NULL,
    password   VARCHAR2(20) NOT NULL
);

ALTER TABLE cliente ADD CONSTRAINT clientes_pk PRIMARY KEY ( id_cliente );

CREATE TABLE curso (
    id_curso    INTEGER NOT NULL,
    descripcion VARCHAR2(200) NOT NULL,
    duracion    VARCHAR2(20) NOT NULL,
    precio      NUMBER NOT NULL
);

ALTER TABLE curso ADD CONSTRAINT cursos_pk PRIMARY KEY ( id_curso );

CREATE TABLE venta (
    id_venta            INTEGER NOT NULL,
    fecha               DATE NOT NULL,
    id_curso            INTEGER NOT NULL,
    id_cliente          VARCHAR2(20) NOT NULL,
    cursos_id_curso     INTEGER NOT NULL,
    clientes_id_cliente VARCHAR2(20) NOT NULL
);

ALTER TABLE venta ADD CONSTRAINT venta_pk PRIMARY KEY ( id_venta,
                                                        fecha );

ALTER TABLE venta
    ADD CONSTRAINT clientes_fk FOREIGN KEY ( clientes_id_cliente )
        REFERENCES cliente ( id_cliente );

ALTER TABLE venta
    ADD CONSTRAINT cursos_fk FOREIGN KEY ( cursos_id_curso )
        REFERENCES curso ( id_curso );