const nav = document.querySelector("#nav");
const abrir = document.querySelector("#abrir");
const cerrar = document.querySelector("#cerrar");

abrir.addEventListener("click", () => {
    nav.classList.add("visible");
})

cerrar.addEventListener("click", () => {
    nav.classList.remove("visible");
});

function registro() {
    window.location = "registro.html" ;
}

function validar_usuario() {
    var expr = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    var nombre = "Andrés Felipe Rivera Calle";
    var correo = document.getElementById("correo");
    var contraseña = document.getElementById("contraseña");
    var correovalido = expr.test(correo.value);

    console.log("nombre: " + nombre);
    console.log("validacion correo: " + correovalido);
    console.log("contraseña: " + contraseña.value);

    if (correo.value == "") {      
        Swal.fire(
            'Ingresar correo',
            'warning'
        )
        console.log("Debe ingresar el correo");
    } else 
        if (!correovalido) {
            Swal.fire(
                'Correo inválido',
                'warning'
            )   
            console.log("Correo no válido: " + correo.value);
        } else 
            if (contraseña.value == "") {
                Swal.fire(
                    'Ingresar contraseña',
                    'warning'
                )
                console.log("Debe ingresar la contraseña");
            } else 
                if (correo.value == "correo123@gmail.com" && contraseña.value == "123") {
                    window.alert("Bienvenido");
                    window.location = "cursos.html?nombre=" + nombre;   
                    console.log("Correo " + correo.value + ", contraseña correcto " + contraseña.value);
                } else 
                if (correo.value !=="correo123@gmail.com" || contraseña.value !== "123") {
                    Swal.fire(
                        '"Usuario y/o Contraseña incorrecto"',
                        'warning'
                    )   
                    console.log("Correo " + correo.value + " ó contraseña incorrecto " + contraseña.value);
    }
}

/*function Mostrar() {
    swal('!Recuperación Exitosa!', 'se ha enviado un correo a and*******@gmail.com con su nueva contraseña', 'success');
}*/