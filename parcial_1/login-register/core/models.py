from django.db import models

# Create your models here.
class Personas(models.Model):
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    documento = models.CharField(max_length=20, unique=True)
    
    def __str__(self):
        return self.id

class Usuarios(Personas):
    id_user = models.AutoField(primary_key=True)
    email = models.CharField(max_length=150, unique=True)
    password = models.CharField(max_length=20)
    fecha_registro = models.DateField
    token = models.CharField(max_length=150)
    username = models.CharField(max_length=150)

    def __str__(self):
        return self.id_user

    def __str__(self):
        return self.nameuser
    
    def __str__(self):
        return self.password
    

