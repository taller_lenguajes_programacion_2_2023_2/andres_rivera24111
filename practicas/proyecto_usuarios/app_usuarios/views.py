# from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render

# Create your views here.

from .models import Usuarios


def index(request):
    # usuarios_list = Usuarios.objects.order_by("id")[:5]
    # output = usuarios_list.values()
    # return HttpResponse(output)

    usuarios_list = Usuarios.objects.order_by("username")[:5]
    template = loader.get_template("app_usuarios/login.html")
    context = {
        "usuarios_listado": usuarios_list,
    }
    return HttpResponse(template.render(context, request))


def login(request):
    # prendas_list = Prendas.objects.all()
    # output = prendas_list.values()
    # return HttpResponse(output)

    usuarios_list = Usuarios.objects.order_by("id")[:5]
    context = {"usuarios_listado": usuarios_list}
    return render(request, "app_usuarios/login.html", context)
