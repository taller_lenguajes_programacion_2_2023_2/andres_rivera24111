# andres_rivera24111

Taller de lenguajes de programación 2

##Entregable 3: 06 de Noviembre 2023

#Instrucciones

##Crear entorno virtual

python -m venv tutorial-env

##Ejecutar entorno virtual

venv/Scripts/activate

##Instalar paquete de requerimientos del proyecto "requirement.txt"

pip install -r requirement.txt

##Realizar migraciones de la base de datos

py manage.py makemigrations
py manage.py migrate

##Levantar el servidor de proyecto

py manage.py runserver

##Usuario y clave inicio de sesión

Email: andres@correo.com
Password: admin123

Herramientas Utilizadas

Google Chrome
GitLab: Sitio oficial
Visual Studio Code: Descargar aquí Extenciones (Python , Git grafh , Git lens)
Freepek: Sitio oficial
Boxicons: Sitio oficial
Draw.io

Control de Versiones

git clone 
git add . Empaquetar cambios
git commit -m "descripcion del cambio" Etiquetar version
git push origin main Carga o empuja el paquete a la ubicacion remota

Información de Inicio de sesión