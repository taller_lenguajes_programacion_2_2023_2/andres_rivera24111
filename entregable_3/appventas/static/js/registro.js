const nav = document.querySelector("#nav");
const abrir = document.querySelector("#abrir");
const cerrar = document.querySelector("#cerrar");
const btnIniciarSesion = document.querySelector("sign-in-btn");

abrir.addEventListener("click", () => {
    nav.classList.add("visible");
})

cerrar.addEventListener("click", () => {
    nav.classList.remove("visible");
});

function Iniciar_sesion() {
    window.location = "login.html" ;
}

function registro() {

    var expr = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    var nombre = document.getElementById("nombre");
    var usuario = document.getElementById("usuario");
    var correo = document.getElementById("correo");
    var contraseña = document.getElementById("contraseña");
    var confirmar_contraseña = document.getElementById("confirmar_contraseña");
    var correovalido = expr.test(correo.value);

    if(nombre.value == "" || usuario.value == "" || correo.value == "" || contraseña.value == "" || confirmar_contraseña.value == "") {
        window.alert("Debe diligenciar todos los campos");
        /*wal.fire(
            'Debe diligenciar todos los campos',
            'warning'
        )*/
    } else
        if(!correovalido){
            window.alert("Correo inválido");
            /*Swal.fire(
                'Correo inválido',
                'warning'
            )*/   
        } else 
            if (contraseña.value !== confirmar_contraseña.value) {
                window.alert("La contraseña no coincide");
                /*Swal.fire(
                    'Las contraseña no coincide',
                    'warning'
                )*/
            } else {
                window.alert("Se ha registrado con éxito");
                window.location = "login.html" ;   
                console.log("nombre: " + nombre.value + ", usuario " + usuario.value + ", contraseña: " + contraseña.value + ", correo: " + correo.value);
            }    
}
