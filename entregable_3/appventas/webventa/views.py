from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse
from .models import Productos
# Create your views here.
def lista(request):
    n_productos = Productos.objects.count()
    lista_prod = Productos.objects.all()
    return render(request,'portal.html',{'n_prod':n_productos,'lista_p':lista_prod})


def detalle_producto(request,id):
    producto = get_object_or_404(Productos,pk=id)
    return render(request,'detalle.html',{'producto':producto})
