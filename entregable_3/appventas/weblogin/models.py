from django.db import models



class AuditoriaFecha(models.Model):
    f_creacion = models.DateTimeField(auto_now_add=True)
    f_actualizar = models.DateTimeField(auto_now=True)
    
    class Meta:
        abstract = True

class Usuario(AuditoriaFecha):
    usuario = models.CharField(max_length=255)
    clave = models.CharField(max_length=255)
    
    def __str__(self) :
        return "Usuario : {} {} {}".format(self.id, self.usuario, self.clave)

class Persona(AuditoriaFecha):
    nombre = models.CharField(max_length=255)
    apellido = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    f_nacimiento=models.DateField()
    usuario = models.ForeignKey(Usuario,on_delete=models.SET_NULL,null=True)
    
    def __str__(self) :
        return "Persona : {} {} {} {}".format(self.id, self.nombre, self.f_nacimiento,self.usuario)