from conexion import Conexion
from prendas import Prendas
import pandas as pd

class Camisas(Prendas):
    def __init__(
        self, id=0, tipo_prenda="", referencia="", descripcion="", planta_confeccion="", tiempo_entrega="", id_camisa=0, tipo_puno="", color="", cantidad=0, promedio_tela=0.0, precio=0.0
    ):
        #super().__init__(id, tipo_prenda, referencia, descripcion, planta_confeccion, tiempo_entrega)
        """Esta es la clase prendas 
        Args:
            id (int, optional): identificador unico de la . Defaults to 0.
            tipo_puno (str, optional): _description_. Defaults to "".
            color (str, optional): _description_. Defaults to "".
            cantidad (int, optional): _description_. Defaults to 0.
            promedio_tela (double, optional): _description_. Defaults to 0.0.
            precio (double, optional): _description_. Defaults to 0.0.
        """       
        self.__id_camisa = id_camisa
        self.__tipo_puno = tipo_puno
        self.__color = color
        self.__cantidad = cantidad
        self.__promedio_tela = promedio_tela
        self.__precio = precio
        super().__init__()
        self.createTable_Camisas()
        ruta = "./entregable_2/proyecto_uniformes/static/xlsx/archivo_datos.xlsx"
        self.df = pd.read_excel(ruta,sheet_name="camisas")
        self.cargaInicial_Camisas()
        self.insert_Camisa()
        self.select_Camisas()
        self.update_Camisas()
        self.select_Camisas()
        self.delete_Camisas()
        self.select_Camisas()

        @property
        def _id_camisa(self):
            return self.__id_camisa

        @_id_camisa.setter
        def _id_camisa(self, value):
            self.__id_camisa = value
            
        @property
        def _tipo_puno(self):
            return self.__tipo_puno

        @_tipo_puno.setter
        def _tipo_puno(self, value):
            self.__tipo_puno = value
            
        @property
        def _color(self):
            return self.__color
        
        @_color.setter
        def _color(self, value):
            self.__color = value
            
        @property
        def _cantidad(self):
            return self.__cantidad

        @_cantidad.setter
        def _cantidad(self, value):
            self.__cantidad = value
            
        @property
        def _promedio_tela(self):
            return self.__promedio_tela

        @_promedio_tela.setter
        def _promedio_tela(self, value):
            self.__promedio_tela = value
            
        @property
        def _precio(self):
            return self.__precio
        
        @_precio.setter
        def _precio(self, value):
            self.__precio = value
    
    def createTable_Camisas(self):
        atributos = vars(self)
        if self.crear_tabla(nom_tabla="camisas",datos_tbl="datos_camisas"):
            print("\nTabla camisas creada\n")
        return atributos
    
    def cargaInicial_Camisas(self):
        """
        The function "cargaInicial_Camisas" loads initial data for clothing items into a database table.
        :return: a boolean value of True.
        """
        datos=""
        for index, row in self.df.iterrows():
            datos = '{},"{}","{}","{}","{}","{}"'.format("NULL",row["tipo_puno"],row["color"],row["cantidad"],row["promedio_tela"],row["precio"])
            self.insertar_datos(nom_tabla="camisas",nom_columns="carga_camisas",datos_carga=datos)
        print("Carga registos de camisas exitosa\n")    
        return True
        
    def insert_Camisa(self):
        """
        The function `insert_Camisas` inserts a record into the "camisas" table with the given data.
        :return: True.
        """
        datos='5,"6 cms","Blanco","14","125","59900"'
        if self.insertar_datos(nom_tabla="camisas",nom_columns="carga_camisas",datos_carga=datos):
            print("Registro insertado en la tabla Camisas\n")
        return True
    
    def select_Camisas(self):
        """
        The function "select_Camisas" retrieves all records from the "camisas" table and prints them.
        :return: a boolean value of True.
        """
        result = self.consultar_datos(nom_tabla="camisas")
        print("Consulta de todos los registros de la tabla camisas\n")
        for x in result:
            print(x)     
        return True
    
    def update_Camisas(self):
        """
        The function updates a record in the "camisas" table with the id 10.
        :return: True.
        """
        nom_tabla = "camisas"
        actualizar = "tipo_puno = 'Redondo', color = 'indigo', cantidad = '12', promedio_tela = '123', precio = '58900'"
        condicion = "id_camisa = 5;"
        self.modificar_datos(nom_tabla,actualizar,condicion)
        print("\nSe actualizó el registro de prendas con el id_Camisa = 5\n")
        return True
    
    def delete_Camisas(self):
        """
        The function `delete_Camisas` deletes a record from the "camisas" table with the id 10.
        :return: a boolean value of True.
        """
        nom_tabla = "camisas"
        condicion = "id_camisa = 5;"
        self.eliminar_datos(nom_tabla,condicion)
        print("\nSe eliminó el registro de camisas con el id_camisa = 5\n")
        return True
