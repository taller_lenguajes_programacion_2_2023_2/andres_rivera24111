# The line `from prendas import Prendas` is importing the `Prendas` class from the `prendas` module.
# This allows the `Prendas` class to be used in the current script.
from prendas import Prendas
from camisas import Camisas

def main():
    """
    The main function initializes an instance of the Prendas class.
    """
    prendas = Prendas()
    camisas = Camisas()

# The `if __name__ == '__main__':` statement is a common idiom used in Python to check if the current
# script is being run as the main module or if it is being imported as a module into another script.
if __name__ == '__main__':
    main()
