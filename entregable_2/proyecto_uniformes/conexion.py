# The `Conexion` class is a Python class that provides methods for connecting to a SQLite database,
# creating tables, inserting data, querying data, modifying data, and deleting data.
import sqlite3
import json

# The `Conexion` class is a Python class that handles database connections and queries using SQLite.
class Conexion:
    
    def __init__(self) -> None:
        """
        The above function initializes the variables and establishes a connection to a SQLite database.
        """
        # The code snippet is initializing instance variables and establishing a connection to a
        # SQLite database.
        self.__user=""
        self.__password=""
        self.__puerto=0
        self.__url=""
        __nom_db="db_uniformes.sqlite"
        self.__querys = self.obtener_json()
        self.__conex = sqlite3.connect(__nom_db)
        self.__cursor = self.__conex.cursor()
        #conex = sqlite3.connect()
    
    def obtener_json(self):
        """
        The function `obtener_json` reads a JSON file containing SQL queries and returns the queries as
        a dictionary.
        :return: a dictionary object named "querys" which contains the data loaded from the JSON file.
        """
        ruta = "./src/poli_proyecto/static/sql/querys.json"
        ruta = "./entregable_2/proyecto_uniformes/static/sql/querys.json"
        querys={}
        with open(ruta, 'r') as file:
            querys = json.load(file) 
        return querys
    
    def crear_tabla(self, nom_tabla="",datos_tbl = ""):
        """
        The function "crear_tabla" creates a table in a database using the provided table name and data.
        
        :param nom_tabla: The parameter "nom_tabla" is the name of the table that you want to create
        :param datos_tbl: The parameter "datos_tbl" is used to specify the name of the query to be used
        for creating the table. It is used to retrieve the necessary data for creating the table from
        the "__querys" dictionary
        :return: a boolean value. If the condition `nom_tabla != ""` is true, it will return `True`.
        Otherwise, it will return `False`.
        """
        if nom_tabla != "" :
            datos=self.__querys[datos_tbl]
            query =  self.__querys["create_tb"].format(nom_tabla,datos)
            self.__cursor.execute(query)
            return True
        else:
            return False
        
    def insertar_datos(self, nom_tabla="", nom_columns = "", datos_carga=""):
        """
        The function `insertar_datos` inserts data into a specified table using the provided column
        names and data.
        
        :param nom_tabla: The parameter "nom_tabla" represents the name of the table where the data will
        be inserted
        :param nom_columns: The parameter "nom_columns" is the name of the columns in the table where
        the data will be inserted
        :param datos_carga: The parameter "datos_carga" represents the data that you want to insert into
        the table. It should be a string containing the values that you want to insert, separated by
        commas. For example, if you want to insert the values "John", "Doe", and 25 into the table
        :return: a boolean value. If the `nom_tabla` parameter is not empty, the function will execute
        an SQL insert query and return `True`. If `nom_tabla` is empty, the function will return
        `False`.
        """
        if nom_tabla != "" :
            columnas=self.__querys[nom_columns]
            query =  self.__querys["insert"].format(nom_tabla,columnas,datos_carga)
            self.__cursor.execute(query)
            self.__conex.commit()
            return True
        else:
            return False
        
    def consultar_datos(self, nom_tabla=""):
        """
        The function "consultar_datos" retrieves data from a specified table in a database.
        
        :param nom_tabla: The parameter "nom_tabla" is a string that represents the name of the table
        you want to query
        :return: the result of the query execution, which is a list of rows fetched from the database
        table specified by the "nom_tabla" parameter.
        """
        if nom_tabla != "" :
            query =  self.__querys["select"].format(nom_tabla)
            self.__cursor.execute(query)
            result = self.__cursor.fetchall()
            self.__conex.commit()
            return result
        else:
            return False   
        
    def modificar_datos(self, nom_tabla="", actualizar="", condicion=""):
        """
        The function modifies data in a specified table based on a given condition.
        
        :param nom_tabla: The parameter "nom_tabla" represents the name of the table in the database
        that you want to update
        :param actualizar: The "actualizar" parameter is used to specify the columns and values that you
        want to update in the table. It should be a string in the format "column1=value1,
        column2=value2, ...". For example, if you want to update the "name" and "age"
        :param condicion: The "condicion" parameter is used to specify the condition that determines
        which rows to update in the table. It is a string that represents the condition in SQL syntax.
        For example, if you want to update rows where the "id" column is equal to 1, the condition would
        be "
        :return: a boolean value of True.
        """
        query =  self.__querys["update"].format(nom_tabla,actualizar,condicion)
        self.__cursor.execute(query)
        self.__conex.commit()
        return True
     
    def eliminar_datos(self, nom_tabla="", condicion=""):
        """
        The function `eliminar_datos` deletes data from a specified table in a database based on a given
        condition.
        
        :param nom_tabla: The parameter "nom_tabla" represents the name of the table from which you want
        to delete data
        :param condicion: The "condicion" parameter is used to specify the condition that determines
        which rows should be deleted from the table. It is a string that represents a SQL WHERE clause.
        For example, if you want to delete all rows where the "age" column is greater than 30, you can
        pass the
        :return: a boolean value of True.
        """
        query =  self.__querys["delete"].format(nom_tabla,condicion)
        self.__cursor.execute(query)
        self.__conex.commit()
        return True