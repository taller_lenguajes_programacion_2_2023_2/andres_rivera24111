# poo
# objeto
# atributos (caratcteristicas) y funciones (acciones)

# diagrama clases      +   diagrama objetos      +    programacion entidades
#
# The code is importing the `Conexion` class from the `conexion` module and the `pandas` library as
# `pd`.
from conexion import Conexion
import pandas as pd

# The `Prendas` class is a subclass of the `Conexion` class and represents a collection of clothing
# items with various attributes and methods for managing the data.
class Prendas(Conexion):
    
    # The `def __init__(self, id=0, tipo_prenda="", referencia="", descripcion="",
    # planta_confeccion="", tiempo_entrega="")` is the constructor method of the `Prendas` class. It
    # initializes the object with the provided arguments or with default values if no arguments are
    # provided.
    def __init__(
        self, id=0, tipo_prenda="", referencia="", descripcion="", planta_confeccion="", tiempo_entrega=""
    ):
        """Esta es la clase prendas 
        Args:
            id (int, optional): identificador unico de la . Defaults to 0.
            tipo_prenda (str, optional): _description_. Defaults to "".
            referencia (str, optional): _description_. Defaults to "".
            descripcion (str, optional): _description_. Defaults to "".
            planta_confeccion (str, optional): _description_. Defaults to "".
            tiempo_entrega (str, optional): _description_. Defaults to "".
        """       
        # The code block is initializing the attributes of the `Prendas` class with the provided
        # arguments or default values. It then calls the constructor of the parent class (`Conexion`)
        # using `super().__init__()`.
        self.__id = id
        self.__tipo_prenda = tipo_prenda
        self.__referencia = referencia
        self.__descripcion = descripcion
        self.__planta_confeccion = planta_confeccion
        self.__tiempo_entrega = tiempo_entrega
        super().__init__()
        self.createTable_Prendas()
        ruta = "./entregable_2/proyecto_uniformes/static/xlsx/archivo_datos.xlsx"
        self.df = pd.read_excel(ruta,sheet_name="prendas")
        self.cargaInicial_Prendas()
        self.insert_Prenda()
        self.select_Prendas()
        self.update_Prendas()
        self.select_Prendas()
        self.delete_Prendas()
        self.select_Prendas()
        
        #atributos = self.create_Per()
        #print(atributos)

        @property
        def _id(self):
            return self.__id

        @_id.setter
        def _id(self, value):
            self.__id = value

        @property
        def _tipo_prenda(self):
            return self.__tipo_prenda

        @_tipo_prenda.setter
        def _tipo_prenda(self, value):
            self.__tipo_prenda = value

        @property
        def _referencia(self):
            return self.__referencia

        @_referencia.setter
        def _referencia(self, value):
            self.__referencia = value

        @property
        def _descripcion(self):
            return self.__descripcion

        @_descripcion.setter
        def _descripcion(self, value):
            self.__descripcion = value

        @property
        def _planta_confeccion(self):
            return self.__planta_confeccion

        @_planta_confeccion.setter
        def _planta_confeccion(self, value):
            self.__planta_confeccion = value

        @property
        def _tiempo_entrega(self):
            return self.__tiempo_entrega

        @_tiempo_entrega.setter
        def _tiempo_entrega(self, value):
            self.__tiempo_entrega = value
        
    def createTable_Prendas(self):
        """
        The function creates a table for "prendas" and returns the attributes of the table.
        :return: the attributes of the object.
        """
        atributos = vars(self)
        if self.crear_tabla(nom_tabla="prendas",datos_tbl="datos_prendas"):
            print("\nTabla prendas creada\n")
        return atributos
    
    def cargaInicial_Prendas(self):
        """
        The function "cargaInicial_Prendas" loads initial data for clothing items into a database table.
        :return: a boolean value of True.
        """
        datos=""
        for index, row in self.df.iterrows():
            datos = '{},"{}","{}","{}","{}","{}"'.format("NULL",row["tipo_prenda"],row["referencia"],row["descripcion"],row["planta_confeccion"],row["tiempo_entrega"])
            self.insertar_datos(nom_tabla="prendas",nom_columns="carga_prendas",datos_carga=datos)
        print("Carga registos de prendas exitosa\n")    
        return True
    
    def insert_Prenda(self):
        """
        The function `insert_Prenda` inserts a record into the "prendas" table with the given data.
        :return: True.
        """
        datos='10,"uniforme aula","514","short niño","La Estrella","8 dias"'
        if self.insertar_datos(nom_tabla="prendas",nom_columns="carga_prendas",datos_carga=datos):
            print("Registro insertado en la tabla Prendas\n")
        return True
    
    def select_Prendas(self):
        """
        The function "select_Prendas" retrieves all records from the "prendas" table and prints them.
        :return: a boolean value of True.
        """
        result = self.consultar_datos(nom_tabla="prendas")
        print("Consulta de todos los registros de la tabla prendas\n")
        for x in result:
            print(x)     
        return True
    
    def update_Prendas(self):
        """
        The function updates a record in the "prendas" table with the id 10.
        :return: True.
        """
        nom_tabla = "prendas"
        actualizar = "tipo_prenda = 'Pañoleta administrativa', referencia = '566', descripcion = 'Pañoleta estampada administrativa', planta_confeccion = 'Medellin', tiempo_entrega = '12 dias'"
        condicion = "id = 10;"
        self.modificar_datos(nom_tabla,actualizar,condicion)
        print("\nSe actualizó el registro de prendas con el id = 10\n")
        return True
    
    def delete_Prendas(self):
        """
        The function `delete_Prendas` deletes a record from the "prendas" table with the id 10.
        :return: a boolean value of True.
        """
        nom_tabla = "prendas"
        condicion = "id = 10;"
        self.eliminar_datos(nom_tabla,condicion)
        print("\nSe eliminó el registro de prendas con el id = 10\n")
        return True
        
    def __str__(self) :
        """
        The above function is a string representation of a class object.
        :return: The method __str__ is returning a string representation of the object.
        """
        return "Prenda: id {}  tipo_prenda {}".format(self.__id,self.__tipo_prenda)